import React, { Component } from "react";
import Details from "../components/Details";
import { Redirect } from "react-router-dom";

class Login extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeUsername = this.handleChangeUsername.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
    this.state = {
      username: "",
      password: "",
      user: null,
      grant_type: "password"
    };
  }

  handleChangeUsername = e => {
    this.setState({
      username: e.target.value
    });
  };

  handleChangePassword = e => {
    this.setState({
      password: e.target.value
    });
  };

  handleSubmit = e => {
    const { username, password, grant_type } = this.state;
    e.preventDefault();
    console.log(username, password, grant_type);
    fetch("http://132.148.19.159/OAuth/Token", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: `username=${username}&password=${password}&grant_type=${grant_type}`
    })
      .then(response => response.json())
      .then(user => {
        console.log("Request success: ", user);
        if (user.access_token !== undefined) {
          this.setState({ user });
          this.setState({ loading: false });
        } else {
          this.setState({ loading: false });
        }
      })
      .catch(function(error) {
        console.log("Request failure", error);
      });
  };

  render() {
    if (this.state.user != null) {
      return <Details />;
    }
    return (
      <div>
        <form onSubmit={this.handleSubmit} className="container">
          <div className="row justify-content-center">
            <div className="col-lg-5 col-md-11 col-min-6">
              <div className="card card-body text-center card-details">
                <h4 className="login-card">Login</h4>
                <div className="form-group mt-3">
                  <label htmlFor="username" className="float-left">
                    Username
                  </label>
                  <input
                    type="text"
                    name="username"
                    className="form-control"
                    id="username"
                    aria-describedby="username"
                    placeholder="Usuario"
                    onChange={this.handleChangeUsername}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="password" className="float-left">
                    Password
                  </label>
                  <input
                    type="password"
                    className="form-control"
                    id="password"
                    aria-describedby="password"
                    placeholder="Password"
                    onChange={this.handleChangePassword}
                  />
                </div>
                <button
                  type="submit"
                  className="btn btn-info btn-lg text-light"
                  onClick={this.handleSubmit}
                >
                  Ingresar
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default Login;
