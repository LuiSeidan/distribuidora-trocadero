import React, { Component } from "react";
import Buttons from "./Buttons";

export default class Details extends Component {
  render() {
    return (
      <div className="container mb-5">
        <section>
          <Buttons />
        </section>
        <h2 className="mt-5 text-details">Details</h2>
        <div className="table-details-container container ">
          <div className="row justify-content-center ">
            <table className="table">
              <thead className="thead-dark">
                <tr>
                  <th scope="col">Fecha Comprobante</th>
                  <th scope="col">N Comprobante</th>
                  <th scope="col">Tipo</th>
                  <th scope="col">Debe</th>
                  <th scope="col">Haber</th>
                  <th scope="col">Saldo</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">
                    <input type="text" />
                  </th>
                  <th scope="row">
                    <input type="text" />
                  </th>
                  <th scope="row">
                    <input type="text" />
                  </th>
                  <th scope="row">
                    <input type="text" />
                  </th>
                  <th scope="row">
                    <input type="text" />
                  </th>
                  <th scope="row">
                    <input type="text" />
                  </th>
                </tr>
                <tr>
                  <th scope="row">
                    <input type="date" id="date" />
                  </th>
                  <th>
                    <h4>0</h4>
                  </th>
                  <th>
                    <h4>SA</h4>
                  </th>
                  <th>
                    <h4>0</h4>
                  </th>
                  <th>
                    <h4>0</h4>
                  </th>
                  <th>
                    <h4>0</h4>
                  </th>
                </tr>
                <tr>
                  <th scope="row">
                    <input type="date" id="date" />
                  </th>
                  <th>
                    <h4>0</h4>
                  </th>
                  <th>
                    <h4>SA</h4>
                  </th>
                  <th>
                    <h4>0</h4>
                  </th>
                  <th>
                    <h4>0</h4>
                  </th>
                  <th>
                    <h4>0</h4>
                  </th>
                </tr>
                <tr>
                  <th scope="row">
                    <input type="date" id="date" />
                  </th>
                  <th>
                    <h4>0</h4>
                  </th>
                  <th>
                    <h4>SA</h4>
                  </th>
                  <th>
                    <h4>0</h4>
                  </th>
                  <th>
                    <h4>0</h4>
                  </th>
                  <th>
                    <h4>0</h4>
                  </th>
                </tr>
                <tr>
                  <th scope="row">
                    <input type="date" id="date" />
                  </th>
                  <th>
                    <h4>0</h4>
                  </th>
                  <th>
                    <h4>SA</h4>
                  </th>
                  <th>
                    <h4>0</h4>
                  </th>
                  <th>
                    <h4>0</h4>
                  </th>
                  <th>
                    <h4>0</h4>
                  </th>
                </tr>
                <tr>
                  <th scope="row">
                    <input type="date" id="date" />
                  </th>
                  <th>
                    <h4>0</h4>
                  </th>
                  <th>
                    <h4>SA</h4>
                  </th>
                  <th>
                    <h4>0</h4>
                  </th>
                  <th>
                    <h4>0</h4>
                  </th>
                  <th>
                    <h4>0</h4>
                  </th>
                </tr>
              </tbody>
            </table>
            <nav aria-label="Page navigation example">
              <ul className="pagination pg-blue">
                <li className="page-item disabled">
                  <span className="btn btn-info btn-lg text-light">
                    Previous
                  </span>
                </li>
                <li className="page-item">
                  <a className="page-link">Page</a>
                </li>
                <input type="text" className="input-details" />
                <li className="page-item">
                  <a className="page-link">of {}</a>
                </li>
                <li className="page-item">
                  <a className="btn btn-info btn-lg text-light">Next</a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    );
  }
}
