import React from "react";

export default function Buttons() {
  return (
    <section id="buttons-details">
      <div className="container">
        <div className="row justify-content-center mt-2">
          <button
            id="btnGroupDrop1"
            type="button"
            className="btn btn-info btn-lg dropdown-toggle text-left"
            data-toggle="dropdown"
            aria-haspopup="true"
          >
            Cliente
          </button>
          <div className="dropdown-menu" aria-labelledby="btnGroupDrop1">
            <a className="dropdown-item" href="#">
              Dropdown link
            </a>
            <a className="dropdown-item" href="#">
              Dropdown link
            </a>
          </div>
          <button
            id="btnGroupDrop1"
            type="button"
            className="btn btn-info btn-lg  dropdown-toggle text-left"
            data-toggle="dropdown"
            aria-haspopup="true"
          >
            Postnet
          </button>
          <div className="dropdown-menu" aria-labelledby="btnGroupDrop1">
            <a className="dropdown-item" href="#">
              Dropdown link
            </a>
            <a className="dropdown-item" href="#">
              Dropdown link
            </a>
          </div>
          <button
            id="btnGroupDrop1"
            type="button"
            className="btn btn-info btn-lg  dropdown-toggle text-left"
            data-toggle="dropdown"
            aria-haspopup="true"
          >
            Fecha
          </button>
          <div className="dropdown-menu" aria-labelledby="btnGroupDrop1">
            <a className="dropdown-item" href="#">
              Dropdown link
            </a>
            <a className="dropdown-item" href="#">
              Dropdown link
            </a>
          </div>
        </div>
      </div>
    </section>
  );
}
